package ru.vadim.activetest.consumer.component;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import ru.vadim.activetest.entity.SystemMessage;

import java.io.Serializable;

@Component
@RequiredArgsConstructor
public class MessageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);

    @JmsListener(destination = "bridgingcode-queue")
    public void messageListener(SystemMessage systemMessage) {
        LOGGER.info("Message received by queue consumer" + systemMessage);
    }

    @JmsListener(destination = "bridgingcode-topic")
    public void messageListener2(SystemMessage systemMessage) {
        LOGGER.info("Message received by topic consumer " + systemMessage);
    }

}
