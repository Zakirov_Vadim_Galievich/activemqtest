package ru.vadim.activetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActiveTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActiveTestApplication.class, args);
    }

}
